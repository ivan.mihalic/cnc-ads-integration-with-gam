# CNC ads integration with GAM

## Popis implementace
Pro korektní zobrazování reklamy je nutné provést implementaci následujícím způsobem:

Do části HTML `<head>` vložít HTML kód s konfigurací. 

Konfigurace je rozdílná pro typ zobrazní (mobil, dekstop nebo responzivní) a typu obsahu stránky (homepage/výpis článků, článek nebo galerie).

Konkrétní atributy pro konfiguraci obdržíte v konfiguračním souboru  [`site-config.json`](site-config.json).

Dále je nutné implementovat samotné reklamní pozice (jejich ukázky jsou ve složce ad-positions).

Všechny reklamní pozice kromě [`leaderboard_1`](./ad-positions/leaderboard_1.html) mají stejnou strukturu s rozdílnými `class` a `id`.

Přehled pozic, které budete implementovat bude uveden v souboru [`ad-position-placement.txt`](ad-positions-placement.txt), který obdržíte.

Implementace reklamních pozic je rozdílná pro typ obsahu `template` (category, article, gallery) a typ zobrazení `webType` (mobile, desktop)

Rozlišujeme dvě sady reklamních pozic, pro mobilní a desktopové zobrazení. Toto nastavení je pro ne-responzivní i responzivní weby s tím, že pokud máte reaponzivní web, tak hodnota atributu webType je `webType="responsive"`. 
Pokud nastavíte `webType` na responsive tak máte možnost nastavit atribut `responsiveBreakpoint`, který určuje jaké reklamní pozice se bodou inicializovat do jakého rozměru (pokud `responsiveBreakpoint` nenastavíte tak výchozí hodnota je 576).

Pozice [`leaderboard_1`](./ad-positions/leaderboard_1.html) musí být umístěna nad obsahem webu a zároveň element, který obaluje obsah musí mít `class="branding-wrapper"`. Pozice [`leaderboard_1`](./ad-positions/leaderboard_1.html) je pozice výhradně pro desktopové zobrazení.


Na pozice ve stránce (mimo sidebar), kde se maji zobrazit opakující se pozice (tzv. [`repeatery`](ad-positions/rectangle_repeater.html)), vložte následující kód:
```html
<div class="cnc_repeater_ad_position"></div>
```
Reklamní skript následně automaticky vyplní tyto placeholdery obsahem reklamy. Je klíčové zajistit, aby tyto prvky nepřejímaly žádné styly z okolního webu.
Tento postup můžete opakovat neomezeně, a to pro všechny typy zobrazení (desktop, mobile, tablet atd.).

Pro pozici Halfpage-1 je vymezen uvnitř bočního panelu (sidebaru) prostor o výšce 1800px. V tomto prostoru zůstává reklamní pozice "sticky" během posouvání stránky. Sticky funkcionalitu zajistuje web.
Ukázka, jak by se dala pozice implementovat [`zde`](ad-functionality-preview/sticky_halfpage_ad.html)




Reálná ukázka HTML konfigurace je v souboru [`page-definition-example.html`](page-definition-example.html).   

## Používané hodnoty v konfiguraci pro reklamu


### Hodnoty v objektu window.__cncPageDefinition

`site = string` _(identifikace webu - najdete v konfiguračním souboru)_

`forceArea = 'homepage' | 'ostatni' | ...` _(typ obsahu: `homepage` = pouze homepage, `ostatni` = všechny ostatní stránky; možnosti pro váš web najdete v konfiguračním souboru)_

`webType = 'mobile' | 'dekstop' | 'responsive'`

`responsiveBreakpoint = null | integer` _(pokud je hodnota `webType = 'responsive'`, tak je možné využít parametr `responsiveBreakpoint`, který určuje zlom mezi mobilního zobrazením a větším, pokud není uvedeno, je výchozí hodnota nastavena na 576px

`keywords = []` _(pole klíčových slov z webu `<meta keywords>`, např: `keywords = ["vanoce", "darky","jezisek"]`)_

`template = 'category' | 'article' | 'gallery'` _(`category` = homepage nebo ostatní vypisy článků, `article` = článek, `gallery` = galerie)_


### Ukázka konfiguračního souboru _(obdržíte vlastní config)_
_[site-config.json](site-config.json)_
```json
{
    "site": "cncenter",
    "forceArea": "homepage" | "ostatni" | "prvniliga"
}
```

### Ukázka soupisu reklamních pozic, které je nutné vložit do webu pro jednotlivé template a typ zobrazení _(obdržíte vlastní soupis)_
_[ad-positions-placement.txt](ad-positions-placement.txt)_
```text
category
    mobile
        mobile_rectangle_2
        mobile_rectangle_3
        mobile_rectangle_4
        out_of_page
    desktop
        leaderboard_1
        commercial_1
        commercial_2
        halfpage_1
        halfpage_2
article
    mobile
        mobile_rectangle_2
        mobile_rectangle_3
        mobile_rectangle_4
        out_of_page
    desktop
        leaderboard_1
        commercial_1
        commercial_2
        halfpage_1
        halfpage_2
gallery
    mobile
        small_leaderboard
    desktop
        leaderboard_1
        halfpage_1
```

### Příklad implementace do webu
```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Title</title>

	<!--    CNC PAGE DEFINITION     -->

	<!-- Styl pro označení reklamních pozic a nastavení jejich výchozích rozměrů. Pokud zde nebude načten, tak si ho script "//w.cncenter.cz/cnc-wrapper.min.js" nahraje sám -->
	<link href="//w.cncenter.cz/styles/cnc-slot-sizes.min.css" rel="stylesheet" id="cnc_global_css">

	<script type="application/javascript">
		window.__cncPageDefinition = window.__cncPageDefinition || {};

		//v pripade SPA (single page application) je nutne volat pri kazdem prechodu mezi strankami funkci pro znovunacteni reklamy - pred tim je nutne zmenit hodnoty v objektu window.__cncPageDefinition vzdy podle cilove stranky (viz. priklad SPA update)
		window.__cncPageDefinition.browserEngine = 'SSR';// SSR | SPA

		window.__cncPageDefinition.site = '{site}';
		window.__cncPageDefinition.template = '{template}'; //category = homepage + ostatni vypisy clanku, article = clanek, gallery
		window.__cncPageDefinition.webType = '{webType}';//mobile, dekstop, responsive
		window.__cncPageDefinition.forceArea = '{forceArea}';//homepage = pouze homepage, ostatni = vse ostatni
		window.__cncPageDefinition.responsiveBreakpoint = '{responsiveBreakpoint}';//integer - breakpoint mezi mobilnim zobrazenim a vetsim, kdyz nebude vyplneno tak vychozi hodnota je 576px
		window.__cncPageDefinition.keywords = [];/* pole klicovych slov (stejne jako je v <meta keywords>), napr: ["vanoce", "darky","jezisek"] */
	</script>

	<!-- 
		níže uvedený skript má varibilní URL dle webu, na který se umísťuje (informace o správné hodnotě pro váš web získáte od CNC)
		tento skript se vkládá pouze, pokud má být na webu i header bidding
		{site_cpex_prebid_settings}
	-->

	<!-- tento skript id=cnc_cpex_cmp zajišťuje načítání CMP lišty -->
	<script id="cnc_cpex_cmp" src="//cdn.cpex.cz/cmp/v2/cpex-cmp.min.js" async="async" fetchpriority="high"></script>

	<script id="cnc_gpt" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js" async="async"></script>

	<!-- tento skript může být nahrazen jiným v případě rozdílné implementace (více info u CNC), popř. nemusí být vkládán -->
	<script id="cnc_cpex_prebid_settings" src="//micro.rubiconproject.com/prebid/dynamic/22918.js" async="async" referrerpolicy="strict-origin-when-cross-origin" ></script>

	<script id="cnc_wrapper" type="application/javascript" src="//w.cncenter.cz/cnc-wrapper.min.js" async="async"></script>

	<!--	CNC PAGE DEFINITION	-->


	<!-- priklad SPA update	-->
	<script type="application/javascript">
		// Zavolat ve chvili prvniho nacteni stranky tak i v pripade prechodu mezi strankami

/* 
Toto je pouze ukázka, jak by mohla vypadat funkce pro aktualizaci __cncPageDefinition a následného zavolání __cncAdsNewAdSlots, která má na starost reload reklamy.
(Funkce __cncAdsNewAdSlots se volá přímo, pokud je splněno __cncAdsInitialized = true, nebo pomocí reklamní fronty __cncAdsQueue.)
Vlastník webu může využít tuto předpřipravenou funkci, nebo si vytvořit vlastní verzi. 
Klíčové však je, aby byla funkce __cncAdsNewAdSlots volána až ve chvíli, kdy je __cncPageDefinition naplněna aktualizovanými daty, a aby toto volání probíhalo prostřednictvím reklamní fronty.
*/

		const changePageAds = (props) => {
			if (!props || typeof props !== 'object') return;

			const pageDefinition = window.__cncPageDefinition || {};
			const configurableFields = ['template', 'webType', 'forceArea', 'responsiveBreakpoint', 'keywords'];

			configurableFields.forEach(key => {
				if (props[key] !== undefined) {
						pageDefinition[key] = props[key];
				}
			});

			window.__cncPageDefinition = pageDefinition;

			const initializeAdSlots = () => {
				if (typeof window.__cncAdsNewAdSlots === 'function') {
						window.__cncAdsNewAdSlots();
				}
			};

			window.__cncAdsInitialized 
				? initializeAdSlots()
				: (window.__cncAdsQueue = window.__cncAdsQueue || []).push(initializeAdSlots);
		};
	
	/* 
	// usage

	const props = {
		template: 'article',	
		forceArea: 'zpravy',	
		keywords: ['keyword1', 'keyword2']
	};

	changePageAds(props);
	*/
	</script>
	<!-- priklad SPA update	-->
</head>
<body>

	<!-- reklamní pozice (leaderboard_1) -->
	<div id="cnc_branding_creative_wrapper">
		<div class="cnc-ads cnc-ads--leaderboard">
			<div class="cnc-ads__within" id="cnc_leaderboard_1"></div>
		</div>
	</div>

	<!-- nutné aby element který obaluje celý obsah webu měl class="branding-wrapper"   -->
	<div class="branding-wrapper">

	<!-- obsah webu + reklamní pozice -->

		<!-- reklamní pozice (halfpage_1) -->
		<div class="cnc-ads cnc-ads--halfpage">
			<div class="cnc-ads__within" id="cnc_halfpage_1"></div>
		</div>

		<!-- reklamní pozice (halfpage_repeater) -->
		<div class="cnc_side_ad_position"></div>

		<!-- reklamní pozice (rectangle_480_1) -->
		<div class="cnc-ads cnc-ads--rectangle_480_1">
			<div class="cnc-ads__within" id="cnc_rectangle_480_1"></div>
		</div>

		<!-- atd. - všechny pozice jsou uvedeny ve složce ad-positions -->

	</div>
</body>
</html>
```

### Příklad implementace do webu s responzivním zobrazováním/skrýváním pozic
```html
<!DOCTYPE html>
<html lang="cs">

	<head>
		<!-- Vlastní implementace stylu pro skrýváni elementů podle šířky zobrazní -->
		<!-- Zadejte vlastní šířku (v ukázce 1080px) breakpointu pro mobilní/desktopové zobrazní -->
		<!-- Pojmenování class u wrapper DIVu .ad-position--mobile/.ad-position--desktop je libovolné, není vázáno na nic jiného, můžete pojmenovat libovolně -->
		<style>
			.ad-position--desktop, .ad-position--tablet, .ad-position--mobile {
				display: none;
			}

			@media only screen and (min-width: 1080px) {
				.ad-position--desktop {
					display: block;
				}
			}
	
			@media (min-width: 768px) and (max-width: 1079px) {
				.ad-position--tablet {
					display: block;
				}
			}

			@media only screen and (max-width: 767px) {
				.ad-position--mobile {
					display: block;
				}
			}
		</style>

	</head>
	<body>
		<!-- wrapper zarucujici, ze se pozice zobrazi pouze pro desktop -->
		<div class="ad-position--desktop">

			<!-- reklamní pozice (leaderboard_1) -->
			<div id="cnc_branding_creative_wrapper">
				<div class="cnc-ads cnc-ads--leaderboard">
					<div class="cnc-ads__within" id="cnc_leaderboard_1"></div>
				</div>
			</div>
		</div>

		<!-- nutné aby element který obaluje celý obsah webu měl class="branding-wrapper" 	-->
		<div class="branding-wrapper">
			<!-- wrapper zarucujici, ze se pozice zobrazi pouze pro desktop -->
			<div class="ad-position--desktop">
		
				<!-- reklamní pozice pro desktopové zobrazení -->
				<div class="cnc-ads cnc-ads--rectangle_480_1">
					<div class="cnc-ads__within" id="cnc_rectangle_480_1"></div>
				</div>
		
			</div>
		
			<!-- wrapper DIVu pro mobilní / tablet zobrazení -->
			<div class="ad-position--mobile ad-position--tablet">
		
				<!-- reklamní pozice pro mobilní zobrazení -->
				<div class="cnc-ads cnc-ads--mobile_rectangle_2">
					<div class="cnc-ads__within" id="cnc_mobile_rectangle_2"></div>
				</div>
		
			</div>
		</div>
	</body>

</html>
```

## Galerie

_v ```diagrams/Galerie.png``` je diagram, ve kterém je vizuálně rozkreslená implementace galerií_

Pro správné fungování galerií je potřeba:
- ```window.__cncPageDefinition.template = "gallery"``` (u galerií ve článku, zůstává ```.template = "article"```)
- přidat wrappery
- přidat volánní pageEventů pro ovládací prvky


### galerie - wrappery

**Desktop**

- Pro správné fungování formátu branding je potřeba hlavní content obalit classou ```"branding-wrapper"```, abychom předešli překrývání obsahu.
```
<div class="branding-wrapper">
<!-- HTML gallerie -->
</div>
```

- Nad obsah se musí přidat kód pro leaderboard_1 pozici
```
<div class="ad-position--desktop">
   <div id="cnc_branding_creative_wrapper">
      <div class="cnc-ads cnc-ads--leaderboard">
         <div class="cnc-ads__within" id="cnc_leaderboard_1"></div>
      </div>
   </div>
</div>
```
**Mobile / Tablet**

- Pro mobilní a tabletové zobrazení stačí vložit reapeater placeholder pod každou třetí fotku.
```
<div class="ad-position--mobile ad-position--tablet">
    <div class="cnc_repeater_ad_position">
    </div>
</div>
```

### galerie - ovládací prvky

- Na všechny ovládací prvky, které střídají fotku je potřeba přidat volání funkce ```window.__cncPageEvent("gallery")```
- Pokud se galerie nachází v **článku**, má page event jinou hodnotu ```window.__cncPageEvent("galleryArticle")```
