category
    mobile
        mobile_rectangle_2
        mobile_rectangle_3
        mobile_rectangle_4
        out_of_page
    desktop
        leaderboard_1
        commercial_1
        commercial_2
        halfpage_1
        halfpage_2
article
    mobile
        mobile_rectangle_2
        mobile_rectangle_3
        mobile_rectangle_4
        out_of_page
    desktop
        leaderboard_1
        commercial_1
        commercial_2
        halfpage_1
        halfpage_2
gallery
    mobile
        small_leaderboard
    desktop
        leaderboard_1
        halfpage_1